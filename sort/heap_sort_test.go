// Copyright 2013 By Jshi. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package sort_test

import (
	ag "Ag/sort"
	"sort"
	"testing"
)

func TestHeapSort(t *testing.T) {
	var ints = [...]int{74, 59, 238, -784, 9845, 959, 905, 0, 0, 42, 7586, -5467984, 7586}
	ag.HeapSort(sort.IntSlice(ints[0:]))
	if !sort.IntsAreSorted(sort.IntSlice(ints[0:])) {
		t.Errorf("Expect sorted: %v", ints)
	}
}
