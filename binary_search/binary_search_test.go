// Copyright 2013 By Jshi. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package binary_search_test

import (
	ag "ag/binary_search"
	"testing"
)

type BinarySearchTest struct {
	Want int
	I    int
}

func TestBinarySearch(t *testing.T) {
	var ints = []int{1, 2, 3, 4, 5, 6, 7, 8, 9}

	var testcases = []BinarySearchTest{
		{9, 8},
		{1, 0},
		{4, 3},
		{10, -1},
	}

	for _, testcase := range testcases {
		if i := ag.BinarySearchInts(ints, testcase.Want); i != testcase.I {
			t.Errorf("Expect %v. Get %v", testcase.I, i)
		}
	}
}
